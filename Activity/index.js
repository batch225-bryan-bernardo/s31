let http = require("http");

const server = http.createServer((request, response) => {

    if (request.url === '/login') {

        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to the login page");

    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end("Page is not available. Sorry");
    }


}).listen(2000)

console.log('Server is running at localhost:2000');